import 'package:flutter/material.dart';

class ChartBar extends StatelessWidget {
  final String label;
  final double spendingAmount;
  final double spendingPctThisWeek;

  ChartBar(this.label, this.spendingAmount, this.spendingPctThisWeek);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constrainsts) {
      return Column(
        children: [
          Container(
            height: constrainsts.maxHeight * 0.15,
            child: FittedBox(
              child: Text('\$ ${spendingAmount.toStringAsFixed(0)}'),
            ),
          ),
          SizedBox(
            height: constrainsts.maxHeight * 0.05,
          ),
          Container(
            height: constrainsts.maxHeight * 0.6,
            width: 10,
            child: Stack(
              children: [
                Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Theme.of(context).primaryColorLight,
                      width: 1,
                    ),
                    color: Color.fromRGBO(220, 220, 220, 1),
                    //color: Theme.of(context).primaryColorLight,
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                FractionallySizedBox(
                  heightFactor: spendingPctThisWeek,
                  child: Container(
                    decoration: BoxDecoration(
                        color: Theme.of(context).primaryColorDark,
                        borderRadius: BorderRadius.circular(10)),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: constrainsts.maxHeight * 0.05,
          ),
          Container(
            height: constrainsts.maxHeight * 0.15,
            child: Text(label),
          ),
        ],
      );
    });
  }
}
